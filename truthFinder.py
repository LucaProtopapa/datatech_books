import pandas as pd
import numpy as np
#from numpy import dot
#from numpy.linalg import norm
from strsimpy.cosine import Cosine
import jellyfish
import json

def compute_confidence_score(data, source_trust, attribute_key):
    '''
    Computa il punteggio di confidenza, utilizando la trust worthiness delle fonti, somma i punteggi
    di ogni fonte per un dato fatto.

    @params:
        data: il dataset su cui operare
        source_trust: un dictionary contenente {fonte : trust worthiness score}
        attribute_key: nome dell'attributo su cui operare nel dataset

    @return:
        Restituisce la tupla (data, confidence) dove data rappresenta il dataset modificato
        e confidence è la lista (colonna del dataframe) di confidenza per i fatti

    FORMULA 2
    '''
    # itera per ogni fonte
    for idx, claim in data.iterrows():
        sources = get_sources_for_claim(data, claim, attribute_key)
        ln_sum = sum([source_trust[src] for src in sources])
        data.at[idx, 'confidence'] = ln_sum
    confidence = data['confidence'].values
    return (data, confidence)

def get_sources_for_claim(data, claim, attribute_key):
    '''
    Estrae tutte le fonti che asseriscono uno specifico fatto

    @params:
        data: il dataset da cui estrarre le informazioni
        claim: il fatto asserito
        attribute_key: colonna del dataset su cui fare inferenza

    @return:
        Restituisce la lista di fonti che asseriscono il fatto in input
    '''
    sources = []
    for i, s in data.iterrows():
        if(s[attribute_key]==claim[attribute_key]):
            sources.append(s['source'])
    return sources

def compute_confidence(df, objects, source_trust, attribute_key):
    '''
    Calcola il punteggio di confidenza 

    @params:
        df: il DataFrame pandas in input
        objects: gli oggetti su cui operare (ISBN)
        source_trust: dictionary conenente i punteggi di affidabilità delle fonti
        attribute_key: colonna del dataset su cui si vuole fare inferenza

    @return:
        Restituisce una lista contenente tutti gli objects_data (oggetti con confidenza)
    '''
    all_objects_data = pd.DataFrame()
    for obj in objects:
        data = df[df['object'] == obj]
        
        # Passo 1. Calcola la confidenza dall'affidabilità delle fonti
        data, confidence = compute_confidence_score(data, source_trust, attribute_key)

        # Passo 2. Calcola la confidenza con similarità tra i fatti
        data, confidence = compute_confidence_score_with_similarity(data, confidence, attribute_key)

        # Passo 3. Calcola il punteggio di confidenza di un dato fatto
        data, confidence = compute_final_confidence(data, confidence)

        all_objects_data = pd.concat([all_objects_data, data])
    return all_objects_data

def compute_confidence_score_with_similarity(data, confidence, attribute_key):
    '''
    Calcola la confidenza con similarità tra i fatti.
    Sfrutta una costante di similarità parametrica.

    @params:
        data: il dataset in input
        confidence: la colonna contenente i punteggi di confidenza
        attribute_key: colonna del dataset su cui si vuole fare inferenza

    @return:
        Restituisce una tupla contenente il dataset modificato e la lista con i nuovi 
        punteggi di confidenza per similarità

    FORMULA 3
    '''
    SIMILARITY_CONSTANT = 0.5
    
    facts_set = data[attribute_key].unique()
    # Crea un dizionario di confidenza associato al fatto
    facts_confidence = {x[attribute_key]: x['confidence'] for _, x in data.iterrows()}
    facts_array = np.array(list(facts_confidence.values()))
    new_facts_array = np.copy(facts_array)
    for i, f in enumerate(facts_set):
        similarity_sum = (1 - SIMILARITY_CONSTANT) * facts_array[i] + SIMILARITY_CONSTANT * sum(
            implicates(f, facts_confidence) * facts_array)
            
        # Aggiorno il punteggio di confidenza nel dataset
        data.loc[data[attribute_key] == f, 'confidence'] = similarity_sum
    return (data, new_facts_array)

def sim_calc(f1, f2):
    '''
    Funzione di calcolo della similarita' proposta dal paper

    @params:
        f1: stringa 1 in input
        f2: stringa 2 in input

    @return:
        Restituisce il punteggio di similarita' proposto dal paper
    '''
    z = len(list(set(list(f1.split(" "))).intersection(list(f2.split(" ")))))
    return z/len(f1)

def cosine_sim(f1, f2):
    '''
    Calcola la cosine similarity tra due stringhe

    @params:
        f1: stringa 1 in input
        f2: stringa 2 in input
    
    @return:
        Restituisce il punteggio di cosine similarity tra le stringhe
    '''
    cosine = Cosine(2)
    p0 = cosine.get_profile(f1)
    p1 = cosine.get_profile(f2)

    return cosine.similarity_profiles(p0, p1)

def implicates(fact, fact_sources):
    '''
    Implementazione della funzione 'implica'.
    Sfrutta la cosine similarity per definire definire il processo di implicazione

    @params:
        fact: fatto da confrontare
        fact_sources: lista di fonti che asseriscono il fatto in input

    @return:
        Restituisce il punteggio di cosine similarity pesato su tutte le fonti 
        che asseriscono il fatto in input

    FORMULA 4
    '''

    BASE_SIMILARITY = 0.5
    result = [cosine_sim(str(fact).lower(), str(f).lower()) - BASE_SIMILARITY for f in fact_sources]

    return result

def compute_final_confidence(data, confidence):
    '''
    Funzione di utility per il calcolo della confidenza finale.
    Sfrutta un damping factor parametrico

    @params:
        data: dataset in input
        confidence: lista contenente i punteggi di confidenza

    @return:
        Restituisce una tupla (data, confidence) dove data rappresenta il dataset modificato e
        confidence la lista dei valori di confidenza aggiornati col damping factor
    '''
    DAMPING_FACTOR = 0.3
    for idx, claim in data.iterrows():
        data.at[idx, 'confidence'] = 1 / (1 + np.exp(-DAMPING_FACTOR * claim['confidence']))

    return (data, confidence)

def compute_source_trust(data, sources):
    '''
    Calcola l'affidabilità delle fonti. Il punteggio di affidabilità è la confidenza media 
    di tutti i fatti asseriti da una data fonte.

    @params:
        data: il dataset in input
        sources: il dizionari cntenente tutte le fonti (unique) ed i relativi punteggi di affidabilità

    @return:
        Restituisce un dizionario contenente le fonti (unique) ed i relativi punteggi di affidabilità aggiornati
    '''
    for source in sources:
            # t_w = t(w) è l'affidabilità della fonte w
            t_w = sum([confidence for confidence in data[data['source'] == source]['confidence'].values]) / len(
                data[data['source'] == source].index)
            # tau_w = tau(w) è il punteggio di affidabilità di una fonte w
            if(t_w==1):
                t_w = 0.999999999999999
            tau_w = -np.log(1 - t_w)
            # Aggiorno il punteggio di affidabilità
            sources[source] = tau_w

    return sources


# --- MAIN ---
data = pd.read_table('./data/preprocessed/processed_book.csv',sep='\t')

#Rimuovere le righe che hanno 'author' == 'n/a'
data = data.fillna('n/a')
data = data.drop(data[data['authors']=='n/a'].index, inplace=False)

# --- INIZIALIZZAZIONE ALGORITMO ---
max_iteration_count = 10
it = 0
error = 99
tol = 0.001 #tolerance
data["confidence"]=""
attribute_key="authors"
objects=data["object"].unique()

# Inizializza la struttura contenente le fonti e la loro affidabilità
source_trustworthiness = {k: -np.log(1 - 0.9) for k in data['source'].unique()}

while error > tol and it < max_iteration_count:
    #source_trustworthiness_old = copy.deepcopy(source_trustworthiness)
    source_trustworthiness_old = source_trustworthiness.copy()

    # STEP 1. Calcola la confidenza di un fatto [FORMULA 2, 3 e 4]
    data = compute_confidence(data, objects, source_trustworthiness, attribute_key)

    # STEP 2. Calcola il punteggio di affidabilità delle fonti [FORMULA 1]
    source_trustworthiness = compute_source_trust(data, source_trustworthiness)
    
    # Controllo di convergenza del processo (basato sull'andamento dell'errore)
    error = 1 - np.dot(list(source_trustworthiness.values()), 
                       list(source_trustworthiness_old.values()) / (np.linalg.norm(list(source_trustworthiness.values())) * 
                                                                    np.linalg.norm(list(source_trustworthiness_old.values()))))
    it = it+1
    print("Iteration "+str(it)+", error: "+str(error))

print("Numero di iterazioni eseguite:")
print(it)

# Esporto come .csv separato da \t il dataset in output
data.to_csv('./data/out/data_output.csv', encoding='utf-8', sep='\t', index=False)

# Esporto come .json la struttura di affidabilità delle fonti
with open('./data/out/sources_trustworthiness.json', 'w') as fp:
    json.dump(source_trustworthiness, fp)

