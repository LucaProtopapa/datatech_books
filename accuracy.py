import numpy as np
import pandas as pd
from strsimpy.cosine import Cosine
import json

def cosine_sim(f1, f2):
    '''
    Calcola la cosine similarity tra due stringhe

    @params:
        f1: stringa 1 in input
        f2: stringa 2 in input
    
    @return:
        Restituisce il punteggio di cosine similarity tra le stringhe
    '''
    cosine = Cosine(2)
    p0 = cosine.get_profile(f1)
    p1 = cosine.get_profile(f2)

    return cosine.similarity_profiles(p0, p1)
    
def string_preprocessing(str):
    '''
    Processa la stringa per uniformarla in vista del confronto

    @params:
        str: la stringa da uniformare

    @return:
        Restituisce la stringa a cui sono stati rimosse ',' ';' '.' ed gli spazi multipli
    '''
    str = str.replace(',', ' ')
    str = str.replace(';', ' ')
    str = str.replace('.', ' ')
    
    str = ' '.join(str.split())

    return str

def remove_initials(str):
    '''
    Rimuove i nomi presentati solo come iniziali (sottostringhe di un solo carattere)

    @params:
        str: la stringa da cui rimuovere le iniziali

    @return:
        Restituisce una stringa a cui vengono rimosse le sottostringhe di un solo carattere
    '''
    valid = []
    for p in str.split():
        if len(p) > 1:
            valid.append(p)
    str = ' '.join(valid)

    return str
    
def select_authors(data):
    '''
    Aggiunge e popola la colonna 'authorsSelected' con i valori individuati 
    dall'esecuzione del TruthFinder.

    @params:
        data: il dataset in input

    @return:
        Restituisce il dataset modificato con l'aggiunta della colonna 'authorsSelected'
    '''
    
    data["authorsSelected"]=""
    objects=data["object"].unique()
    for book in objects:	
        sources = {fonte : data[(data['source'] == fonte) & (data['object']== book)]['confidence'].values[0] for fonte in data[data['object']== book]['source'].values}
        #elenco di tuple <source - confidence> per il dato libro
        max = 0
        best_source = ''
        for f in sources:
            if (sources[f] > max):
                max = sources[f]
                best_source = f #per il dato libro
        data.loc[data['object']==book,'authorsSelected'] = data[(data['object']==book) & (data['source'] == best_source)]['authors'].values[0]

    return data

def accuracy(data, standard):
    '''
    Calcolo dell'accuracy secondo diverse metodolige e metriche

    @params:
        data: il dataset in input
        standard: il dataset contenente lo standard di confronto (Golden Standard)
    
    @return:
        Restituisce un dictionary contenente tutti i punteggi di accuracy calcolati
    '''
    correct = 0
    correct_no_int = 0
    wrong_order = 0
    wrong_order_no_int=0
    strict_inclusion = 0
    not_found = 0
    threshold = 0.95
    proportional_accuracy = 0
    
    for book in standard['object']:
        # La stringa 'a' contiene gli autori risultanti dalla data fusion tramite l'algoritmo TruthFinder
        # La stringa 'b' contiene gli autori indicati nello standard
        a = data[data['object']==book]['authorsSelected'].values
        if (len(a)!=0):
            a = a[0].strip().lower()
            b = standard[standard['object']==book]['authors'].values[0].strip().lower()
            
            a = string_preprocessing(a)
            b = string_preprocessing(b)

            sim = cosine_sim(a,b)
            
            if (sim >= threshold):
                correct +=1
            else:
                liA = list(a.split(" "))
                liB = list(b.split(" "))
                #stesse parole ma in ordine diverso
                if(sorted(liA)==sorted(liB)):
                    correct+=1
                    wrong_order +=1
            
            aNoInit = remove_initials(a)
            bNoInit = remove_initials(b)
            
            sim_no_init = cosine_sim(aNoInit,bNoInit)
            
            if (sim_no_init >= threshold):
                correct_no_int +=1
            else:
                liA = list(aNoInit.split(" "))
                liB = list(bNoInit.split(" "))
                if(sorted(liA)==sorted(liB)):
                    correct_no_int+=1
                    wrong_order_no_int +=1
                elif(all(elem in liB  for elem in liA)):
                        strict_inclusion+=1
                        
            data_list = list(aNoInit.split(" "))
            standard_list = list(bNoInit.split(" "))
            proportional_accuracy += len(list(set(data_list).intersection(standard_list)))/(max(len(data_list), len(standard_list)))
        else:
            not_found +=1

    accuracy = {'AccuracyStandard' : 
                    {'Accuracy' : correct / len(standard),
                    'Accuracy_orderFree' : wrong_order},
                'AccuracyNoInitials' :
                    {'Accuracy': correct_no_int / len(standard),
                    'Accuracy_orderFree' : wrong_order_no_int},
                'AccuracyInclusioneStretta' :
                    {'Accuracy' : (correct_no_int + strict_inclusion) / len(standard),
                        'Fonti paziali' : strict_inclusion},
                'AccuracyProporzionale' : round((proportional_accuracy / len(standard)),2)}
    print('N Libri non trovati: ' + str(not_found))
    return accuracy
            
# --- EXEC ---
standardGold = pd.read_table('./data/book_golden_standard.txt',sep='\t',header = None)
standardGold.columns =  ["object", "authors"]

data = pd.read_table('./data/out/data_output.csv',sep='\t')
data = select_authors(data)

result = accuracy(data, standardGold)
print(result)

# Salvo il rusltato (dictionary) del calcolo dell'accuracy su file .json
with open("./data/analisys/accuracy_book.json", "w") as outfile:  
    json.dump(result, outfile)