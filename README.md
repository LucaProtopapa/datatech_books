# DataTech_Books


## Dipendenze
Come librerie aggiuntive abbiamo utilizzato:
- pandas
- numpy
- jellyfish (v >= 8.0)
- strsimpy
- matplotlib

Per installarle è sufficiente eseguire il comando `pip install -r requirements.txt`

## Esecuzione del progetto
La prima operazione è quella di preprocessing, seguita dall'analisi del dataset che ne evidenzia le modifiche.
E' poi possibile eseguire il processo di Data Fusion tramite l'algoritmo TruthFinding eseguendo l'omonimo file.
In seguito alla sua esecuzione, è possibile processare e visualizzare i risultati ottenuti dal punto di vista dell'accuracy, della Data Fusion e della copertura ed affidabilità delle fonti.

Per ottenere i risultati indicati è sufficiente eseguire il seguente file
`> project_data_fusion.py`

In alternativa è possibile seguire questo workflow:

`> preprocessing.py` <br>
`> dataAnalisys.py` <br>
`> truthFinder.py` <br>
`> accuracy.py` <br>
`> dataFusionResults.py` <br>
`> sourcesCoverage.py` <br>
