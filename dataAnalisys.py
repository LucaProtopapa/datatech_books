import pandas as pd
import numpy as np

def analysis(data):
	'''
	Calcola la tabella di analisi del dataset, composta da
	count, unique, top, frequency

	@params:
		data: il dataset da analizzare
	
	@return:
		restituisce un dataframe contenente la tabella di analisi
	'''
	analysis = pd.DataFrame(columns = data.columns, index = ["count", "unique", "top", "freq"])

	for i in data.columns:
		analysis[i]['count'] = data[i].count()
		analysis[i]['unique'] = data[i].nunique()
		analysis[i]['top'] = data[data[i] != None].mode()[i][0]
		analysis[i]['freq'] = data[data[i] == analysis[i]['top']][i].count()
		
	print(analysis)
	return (analysis)

# --- EXEC ---
data = pd.read_table('./data/book.txt',sep='\t',header = None)
data.columns =  ["source", "object", "title", "authors"]
analysis_table = analysis(data)
analysis_table.to_json('./data/analisys/dataAnalisys_book.json')

after_preprocessing = pd.read_table('./data/preprocessed/processed_book.csv',sep='\t')
analysis_after = analysis(after_preprocessing)
analysis_after.to_json('./data/analisys/dataAnalisys_book_preprocessed.json')