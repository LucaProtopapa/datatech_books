#Script per il plot della dataFusion
import pandas as pd
import matplotlib.pyplot as plt
import json
import itertools


def plot_fusion_errorRate(error_list, output_path):
    '''
    Plotta la tendenza degli errori ad ogni iterazione dell'algoritmo.

    @params:
        error_list: lista contenente l'errore. Ad una posizione i corrisponde l'iterazione i+1
        output_path: percorso di output su cui salvare il grafico come .png

    @return:
        Restituisce -1 se non sono presenti elementi all'interno della lista di errori, 0 altrimenti.
    '''
    if(len(error_list)==0):
        return -1

    x_axis = []
    for i in range(0, len(error_list)):
        x_axis.append(i)

    plt.plot(x_axis, error_list)
    plt.xticks(x_axis)
    plt.ylabel('error')
    plt.xlabel('iteration')
    plt.title('Data Fusion error/iteration')
    for i,j in zip(x_axis, error_list):
        #plt.annotate(str(j),xy=(i,j+0.003))
        plt.text(i, j+0.003, str(j), fontsize=6, horizontalalignment='center', color='red')

    plt.savefig(output_path)
    plt.show()

    return 0

def plot_fusion_sourcesByTrust(input_path, output_path1, output_path2, output_path_file):
    ''''
    Plotta i risultati della data fusion, mostrando le fonti più e meno affidabili (top e bottom 10)

    @params:
        input_path: percorso contenente il dataset in input
        output_path1: percorso su cui salvare il grafico contenente le 10 
                        migliori fonti per affidabilità, come .png
        output_path2: percorso su cui salvare il grafico contenente le 10 
                        peggiori fonti per affidabilità, come .png
        output_path_file: percorso su cui salvare il dictionary contenente i dictionary 
                            che rappresentano le top e bottom 10 fonti per affidabilità
    @return:
        Restituisce il dictionary contenente i dictionary che rappresentano 
        le top e bottom 10 fonti per affidabilità
    '''
    sources_trust = {}
    with open(input_path, 'r') as f:
        sources_trust = json.load(f)

    sources_trust_top = {k: v for k, v in sorted(sources_trust.items(), key=lambda item: item[1], reverse=True)}
    sources_trust_top = dict(itertools.islice(sources_trust_top.items(), 10))

    sources_trust_bottom = {k: v for k, v in sorted(sources_trust.items(), key=lambda item: item[1], reverse=False)}
    sources_trust_bottom = dict(itertools.islice(sources_trust_bottom.items(), 10))

    sources_trust_out = {'top_trust' : sources_trust_top, 'bottom_trust' : sources_trust_bottom}

    with open(output_path_file, "w") as outfile:  
        json.dump(sources_trust_out, outfile)

    data = sources_trust_top.items()
    sources, trust = zip(*data)

    data2 = sources_trust_bottom.items()
    sources2, trust2 = zip(*data2)

    plt.bar(sources, trust, color='#336600', edgecolor='#003300')
    plt.xticks(rotation=90)
    plt.subplots_adjust(bottom=0.25)
    plt.savefig(output_path1)
    plt.show()

    plt.bar(sources2, trust2, color='#336600', edgecolor='#003300')
    plt.xticks(rotation=90)
    plt.subplots_adjust(bottom=0.25)
    plt.savefig(output_path2)
    plt.show()

    return sources_trust_out

# --- EXEC ---

#Error rate per iteration
fusion_errorRate = [0.17760334144736079, 0.018367642958323382, 0.004157365040926342, 0.0022486142361741734, 0.001009973564483846, 0.0005889536766721104]
output_path = './data/out/fusion_error.png'

plot_fusion_errorRate(fusion_errorRate, output_path)

#Sources trustworthiness
in_path = './data/out/sources_trustworthiness.json'
out_path1 = './data/out/top_sources_trustworthiness.png'
out_path2 = './data/out/bottom_sources_trustworthiness.png'
output_path_file = './data/out/sources_trustworthiness_scores.json'

plot_fusion_sourcesByTrust(in_path, out_path1, out_path2, output_path_file)