#Preprocessing

'''
##Preprocessing RULES:
#--- TITOLI ---
#Rimuovo tutti gli apostrofi "'"
#Trasformo tutti gli '&' e gli 'and' in ', '
#Trasformo '/' in ', '
#Trasformo '~' in ', '

#Infine rimuovo gli spazi multipli e quelli prima e dopo la stringa
#Se dopo questa fase, un'intero campo risulta vuoto, viene sostituito con 'N/A'

#--- AUTORI ---
#Rimuovo tutti i '.'
#Rimuovo 'Author'
#Rimuovo 'Editor'
#Rimuovo 'Edit'
#Trasformo tutti gli '&' e gli 'and' in ', '
#Trasformo '/' in ', '
#Trasformo '~' in ', '

#Se compare testo tra parentesi, lo rimuovo '(...)'
#Se compare un 'By' -> rimuovere ',', mettere ', ' al posto di 'By' a meno che il By non è all'inizio della stringa
#Se compare una ',' come ultimo carattere della stringa, la rimuovo
#Se compare una ',' come primo carattere della stringa, la rimuovo
#Se compare un ';' tolgo ',' e sostituisco ';' con ','

#Infine rimuovo gli spazi multipli e quelli prima e dopo la stringa
#Se dopo questa fase, un'intero campo risulta vuoto, viene sostituito con 'N/A'
'''

import pandas as pd

def check_parentheses(s):
    '''
    Esegue un controllo sull'accoppiamento delle parentesi nella stringa in input

    @params:
        s: stringa da controllare
    @return:
        True se le parentesi in s sono accoppiate, False altrimenti
    '''
    """ Return True if the parentheses in string s match, otherwise False. """
    j = 0
    for c in s:
        if c == ')':
            j -= 1
            if j < 0:
                return False
        elif c == '(':
            j += 1
    return j == 0


def find_parentheses(s):
    '''
    Trova gli indici delle parentesi accoppiate all'interno della stringa in input

    @params:
        s: stringa da cui estrarre le parentesi

    @return:
        Restituisce una lista di tuple, ognugna delle quali contiene una coppia di indici di s

    @raise:
        IndexError: se le parentesi non sono accoppiate correttamente
    '''
    
    # Gli indici delle parentesi aperte sono immagazzinati in una lista (stack)
    stack = []
    parentheses_locs = {}
    for i, c in enumerate(s):
        if c == '(':
            stack.append(i)
        elif c == ')':
            try:
                parentheses_locs[stack.pop()] = i
            except IndexError:
                raise IndexError('Too many close parentheses at index {}'.format(i))
    if stack:
        raise IndexError('No matching close parenthesis to open parenthesis at index {}'.format(stack.pop()))
    return parentheses_locs

def remove_internal_parentheses(string):
    '''
    Controlla se le parentesi sono accoppaite, se lo sono, le toglie eliminando il loro contenuto
    altrimenti le toglie come singoli char

    @params:
        string: stringa da cui rimuovere il contenuto
    @return:
        Restituisce la stringa con le parentesi, ed il loro contenuto, rimosse
    '''
    if (check_parentheses(string)):
        parentheses = find_parentheses(string)
        to_be_removed = []
        for pair in parentheses:
            for other in parentheses:
                if ((pair != other) and pair >  other and parentheses[pair] < parentheses[other]):
                    to_be_removed.append(pair)
        for x in to_be_removed:
            del parentheses[x]

        if(parentheses):
            for el in list(reversed(sorted(parentheses.keys()))):
                string = string[0 : el : ] + string[parentheses[el] + 1 : :]
    else:
        string = string.replace('(', ' ')
        string = string.replace(')', ' ')
    return string


def book_preprocessing(data_path, output_path):
    '''
    Esegue il preprocessing sul dataset, segue le regole descritte all'inizio del file

    @params:
        data_path: percorso relativo al dataset in input
        output_path: percorso relativo all'output su cui salvare il dataset processato
    
    @return
        Restituisce il dataset processato sotto forma di DataFrame pandas
    '''
    data = pd.read_table(data_path, sep='\t', header = None)
    data.columns =  ["source", "object", "title", "authors"]

    #replace Nan
    data = data.fillna('N/A')
    data = data.apply(lambda x: x.str.strip()).replace('', 'N/A')
    data = data.replace(['Not Available', 'not available', 'Not available', 'Not Available (NA)', 'Not Available (NA).', 'n/a', 'N/A', '0', 0, None], 'N/A')

    idx=0
    for cell in data['title']:
        string = str(cell).lower()
        #Rimuovo gli spazi iniziali e finali
        string = string.strip()

        #Rimuovo ' e "
        string = string.replace('\'', ' ')
        string = string.replace('\"', ' ')

        #Sostituisco connettori
        string = string.replace('&',', ')
        string = string.replace(' and ',', ')
        string = string.replace('/',', ')
        string = string.replace('~',', ')

        #Rimuovo i doppi spazi
        string = ' '.join(string.split())
        string = string.strip()

        if(string == '' or string=='n, a' or string=='n/a' or string=='n,a'):
            string = 'n/a'

        data['title'][idx] = string
        idx +=1
        
    idx=0
    for cell in data['authors']:
        string = str(cell).lower()
        #Rimuovo gli spazi iniziali e finali
        string = string.strip()

        #Rimuovo caratteri
        string = string.replace('.',' ')
        string = string.replace('author',' ')
        string = string.replace('(joint author)',' ')
        string = string.replace('editor',' ')
        string = string.replace('edit',' ')
        string = string.replace('"', ' ')

        #Sostituisco connettori
        string = string.replace('&',', ')
        string = string.replace(' and ',', ')
        string = string.replace('/',', ')
        string = string.replace('~',', ')

        #Casi speciali        
        ## Caso '(...)'
        string = remove_internal_parentheses(string)

        ## Caso ';' and ','
        if(string.find(';') != -1):
            string = string.replace(',', ' ')
            string = string.replace(';', ', ')

        ## Caso 'By'
        if(string.find('by') == 0 or string.find('by') == 1):
            string = string.replace('by ', ' ')
        elif(string.find('by') != -1):
            string = string.replace(',', ' ')
            string = string.replace(' by ', ', ')

        ## Caso ',' come ultimo char
        if(string[len(string.strip())-1] == ','):
            string = string.strip()[:-1]

        ## Caso ',' come primo char
        if(string.strip()[0] == ','):
            string = string.strip()[1:]

        #Rimuovo i doppi spazi
        string = ' '.join(string.split())
        string = string.strip()

        if(string == '' or string=='n, a' or string=='n/a' or string=='n,a'):
            string = 'n/a'

        #cell = string
        data['authors'][idx] = string
        idx+=1

    #Conteggio duplicati
    print("Numero di righe duplicate rimosse:")
    print(len(data[data.duplicated()]))

    #rimozione delle righe duplicate
    data = data.fillna('N/A')
    data = data.drop_duplicates()

    data.to_csv(output_path, encoding='utf-8', sep='\t', index=False)
    return data

in_path = './data/book.txt'
out_path = './data/preprocessed/processed_book.csv'

book_preprocessing(in_path, out_path)