import pandas as pd
import json
import itertools
import matplotlib.pyplot as plt

def sources_coverage(data_path, output_path, output_csv):
    '''
    Associa ad ogni fonte il numero di libri (unique) che essa tratta.
    Salva su file sia il risultato numerico che quello percentuale

    @params
        data_path: percorso del file in input
        output_path: percorso dell'output associato al json contenente {fonte : value}
        output_csv: percorso dell'output associato al csv contenente {fonte : value_percent}

    @return
        restituisce un dictionary contenente {fonte : value} dove value è il 
        numero di libri (unique) coperti dalla fonte associata
    '''
    data = pd.read_table(data_path, sep='\t')
    books_number = data['object'].nunique()

    coverage = {}
    for s in data['source'].unique():
        coverage[s] = len(data[data['source']==s]['object'].unique())

    with open(output_path, "w") as outfile:  
        json.dump(coverage, outfile)
    
    with open(output_csv, "w") as outcsv:
        for key, value in coverage.items():
            outcsv.write(key+"\t"+str(value*100/books_number)+"\n")

    return coverage

def plot_results(coverage):
    '''
    Plotta le 10 fonti contenenti il maggior numero di libri (unique).
    
    @params:
        coverage: dictionary contenente {fonte : value} dove value è il 
        numero di libri (unique) coperti dalla fonte associata
    
    @return:
        restituisce una lista di tuple (fonte,valore) rappresentante le 10
        fonti con coverage maggiore. Ovvero i dati plottati.
    '''
    coverage = {k: v for k, v in sorted(coverage.items(), key=lambda item: item[1], reverse=True)}
    top_coverage = dict(itertools.islice(coverage.items(), 10))

    data = top_coverage.items()
    sources, frequency = zip(*data)

    plt.bar(sources, frequency, color='#336600', edgecolor='#003300', bottom=0.23)
    plt.xticks(rotation=90)
    plt.subplots_adjust(bottom=0.23)
    plt.savefig('./data/analisys/top10CoverageGraph.png')
    plt.show()

    return data

# --- EXEC ---
in_path = './data/out/data_output.csv'
out_path = './data/analisys/coverage.json'
output_csv = './data/analisys/coverage_percent.csv'
plot_results(sources_coverage(in_path, out_path, output_csv))